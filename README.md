Leo Huang  

DFAPM uses a DFA (see https://en.wikipedia.org/wiki/Deterministic_finite_automaton) to find all occurrences of a pattern P in a text T.  

There must be a txt file named "pattern" and one named "text" in the same directory as DFA.exe.  
Run by double-clicking the exe file, or by entering './DFA' on the command line.  

The program outputs 2 files:    
    1. delta.txt: The transition rules for the DFA. On each line, there are 3 values - an initial state, a character, and a resulting state.  
    2. matches.txt: The indexes of the first character of each occurrence of P.  
