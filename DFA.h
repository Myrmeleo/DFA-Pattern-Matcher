#ifndef DFA_PATTERN_MATCH
#define DFA_PATTERN_MATCH

/* 
DFAPM finds all occurences of pattern P in text T using a DFA
The DFA is defined as follows:
    States are {0, 1, 2,...m} with m being the length of P. 
    The DFA being in state q means that the first q chars of P are the most recent chars we've read from T so far
    Transition rules are of the form (q C d(q, C)) where q is the starting state, C is the next char in T,
        and d is a transition function that gives a resulting state after an initial state q and char C
    
The program reads in the characters of T in sequence, adjusting state each time according to the transition rules
It reports an occurence of P when the state is m
*/
class DFAPM {
    //the pattern to be found in the text
    std::string P;
    //length of P
    int m;
    //the text to be searched for the pattern
    std::string T;
    //the set of characters that appear in P
    std::vector<char> S;
    //the transition rules - A[i] stores all the rules starting at state i, which are represented by (C, d(i, C))
    std::vector<std::vector<std::pair<char, int>>> A;

    //LongestPreSuf(U) returns the longest prefix of P that is a suffix of U
    int longestPreSuf(std::string U);

public:
    //params: pText is the txt file holding the pattern P, tText holds the text T
    DFAPM(std::string pText, std::string tText);

    //writeTransitions() writes to a file the transition rules stored in A
    void writeTransitions();

    //matchToText() finds all occurences of P in T, and outputs the indexes and transition rules to text files
    void matchToText();
};

#endif