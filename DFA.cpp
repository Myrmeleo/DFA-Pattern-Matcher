#include <cstdlib>
#include <vector>
#include <iostream>
#include <fstream>
#include <math.h>
#include <algorithm>
#include "DFA.h"

using namespace std;

DFAPM::DFAPM(std::string pText, std::string tText) {
    //to read in the pattern to match
    ifstream patternF(pText);
    if (patternF.is_open()) {
        if (!getline(patternF, P)) {
            cerr << "No pattern provided. Exiting." << endl;
            exit(-1);
        }
        patternF.close();
    } else {
        cerr << "No file named 'pattern.txt'. Exiting." << endl;
        exit(-1);
    }

    //to read in the text to process
    ifstream textF(tText);
    if (textF.is_open()) {
        if (!getline(textF, T)) {
            cerr << "No text provided. Exiting." << endl;
            exit(-1);
        }
        textF.close();
    } else {
        cerr << "No file named 'text.txt'. Exiting." << endl;
        exit(-1);
    }

    m = P.length();

    //fill S with all the characters that appear in P
    for (int i = 0; i < m; i++) {
        char c = P[i];
        bool found = false;
        //check if c is already recorded in S
        for (char d : S) {
            if (d == c) {
                found = true;
            }
        }
        if (!found) {
            S.push_back(c);
        }
    }

    //write the transition rules into A
    A.resize(m + 1);
    //for each state i of the DFA, we have a transition rule for each C in S
    for (int i = 0; i <= m; i++) {
        for (char C : S) {
            //find the resulting state of the DFA if we were at state i and read in C
            string resultStr = P.substr(0, i) + C;
            int resultState = longestPreSuf(resultStr);
            pair<char, int> rule (C, resultState);
            //add to bucket of transition rules at A[i]
            A[i].push_back(rule);
        }
    }
}

int DFAPM::longestPreSuf(string U) {
    int k = U.length();
    int i = 0;
    int maxL = 0;
    while (i < k && i < m) {
        if (P.substr(0, i + 1) == U.substr(k - 1 - i, i + 1)) {
            maxL = i + 1;
        }
        i += 1;
    }
    return maxL;
}

void DFAPM::writeTransitions() {
    //stream to write out the rules of the transition function
    ofstream deltaF("delta.txt");
    if (deltaF.is_open()) {
        for (int i = 0; i <= m; i++) {
            for (pair<char, int> p : A[i]) {
                deltaF << i << " " << p.first << " " << p.second << endl;
            }
        }
    }
    deltaF.close();
}

void DFAPM::matchToText() { 
    //stream to write out the indices in T for each occurence of P
    ofstream matchF("matches.txt");
    //index we are currently at in T; increments for each character we process
    int i = 0;
    int n = T.length();
    //current state of the DFA
    int q = 0;
    while (i < n) {
        char c = T[i];
        //look for the transition rule (C, d(q, C)) in A[q] that gives the resulting state after c
        for (pair<char, int> p : A[q]) {
            if (p.first == c) {
                q = p.second;
                break;
            } else {
                //q = 0 after we exit the loop only if c is not a character that appears in P
                q = 0;
            }
        }
        i++;
        //if we find an occurence of P in T, report the index of the starting character
        if (q == m) {
            matchF << (i - m) << endl;
        }
    }
    matchF.close();
}

int main() {
    DFAPM dfa = DFAPM("pattern.txt", "text.txt");
    dfa.writeTransitions();
    dfa.matchToText();

    exit(0);
}